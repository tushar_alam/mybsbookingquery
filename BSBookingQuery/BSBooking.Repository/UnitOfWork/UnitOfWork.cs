﻿using BSBooking.Repository.Context;

namespace BSBooking.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BookingDbContext _bookingDbContext;
        public UnitOfWork(BookingDbContext bookingDbContext)
        {
            _bookingDbContext = bookingDbContext;
        }

        public bool SaveChanges()
        {
            return _bookingDbContext.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _bookingDbContext.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            _bookingDbContext?.Dispose();
        }

    }
}
