﻿using BSBooking.Entity.Models;
using Microsoft.EntityFrameworkCore;

namespace BSBooking.Repository.Context
{
    public static class BookingDBInitializer
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserType>().HasData(
              new UserType() { Id = 1, Name = "Admin", Code = "ADMIN" },
              new UserType() { Id = 2, Name = "Customer", Code = "CUSTOMER" }
               );

            modelBuilder.Entity<User>().HasData(
              new User() { Id = 1, Name = "Tofail Alam", UserName = "system_user", Password = "1234", UserTypeId = 1 },
               new User() { Id = 2, Name = "Customer User 1", UserName = "customer_user_1", Password = "1234", UserTypeId = 2 },
                new User() { Id = 3, Name = "Customer User 2", UserName = "customer_user_2", Password = "1234", UserTypeId = 2 }
              );

            modelBuilder.Entity<Location>().HasData(
             new Location() { Id = 1, Name = "Cox's Bazar", SubName = "Bangladesh", Lat = 23.8103, Lon = 90.4125 },
             new Location() { Id = 2, Name = "Dhaka", SubName = "Bangladesh", Lat = 21.4272, Lon = 92.0058 }
              );

            modelBuilder.Entity<Hotel>().HasData(
                new Hotel() { Id = 1, Name = "Lighthouse Family Retreat", LocationId = 1, Rating = 3, Image = null, CreatedBy = "system_user", CreateDate = DateTime.Now },
                new Hotel() { Id = 2, Name = "Hotel Coral Sea", LocationId = 1, Rating = 3, Image = null, CreatedBy = "system_user", CreateDate = DateTime.Now },
                new Hotel() { Id = 3, Name = "Joltorongo", LocationId = 1, Rating = 5, Image = null, CreatedBy = "system_user", CreateDate = DateTime.Now },
                new Hotel() { Id = 4, Name = "Hotel Radison Blue", LocationId = 2, Rating = 5, Image = null, CreatedBy = "system_user", CreateDate = DateTime.Now },
                new Hotel() { Id = 5, Name = "Hotel Shonar ga", LocationId = 2, Rating = 7, Image = null, CreatedBy = "system_user", CreateDate = DateTime.Now }
          );

        }
    }
}
