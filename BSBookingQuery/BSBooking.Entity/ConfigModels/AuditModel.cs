using System.ComponentModel.DataAnnotations;

namespace BSBooking.Entity.ConfigModels
{
    public class AuditModel : EntityModel
    {
        [Required]
        public string CreatedBy { get; set; }
        
        [Required]
        public DateTime CreateDate { get; set; }
        
        public string? LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }

    }
}
