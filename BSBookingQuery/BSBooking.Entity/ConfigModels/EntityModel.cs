﻿using BSBooking.Entity.IModels;
using System.ComponentModel.DataAnnotations;

namespace BSBooking.Entity.ConfigModels
{
    public class EntityModel : IEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
