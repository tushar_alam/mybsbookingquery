﻿using BSBooking.Entity.Models;

namespace BSBooking.Entity.ViewModels
{
    public class HotelModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LocationId { get; set; }
        public Location? Location { get; set; }
        public int Rating { get; set; }
        public string? Image { get; set; }
        
        public string? CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public ICollection<Comment>? Comments { get; set; }
    }
}
