﻿namespace BSBooking.Entity.ViewModels
{
    public class SearchModel
    {
        public int? LocationId { get; set; }
        public int? HotelId { get; set; }
        public string HotelName { get; set; }
        public int? StartRating { get; set; }
        public int? EndRating { get; set; }
    }
}
