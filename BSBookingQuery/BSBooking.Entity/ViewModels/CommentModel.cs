﻿using BSBooking.Entity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSBooking.Entity.ViewModels
{
    public class CommentModel
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public User? User { get; set; }

        public int HotelId { get; set; }
        public Hotel? Hotel { get; set; }

        public string CommentText { get; set; }

        public int? ParentCommentId { get; set; }
        public Comment? ParentComment { get; set; }

    }
}
