﻿namespace BSBooking.Entity.ViewModels
{
    public class ResponseModel<T>
    {
        public string RspnCode { get; set; }
        public string RspnMessage { get; set; }
        public T RspnData { get; set; }
    }
}
