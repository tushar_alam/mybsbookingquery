﻿using BSBooking.Entity.ConfigModels;
using System.ComponentModel.DataAnnotations;

namespace BSBooking.Entity.Models
{
    public class Hotel : AuditModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int LocationId { get; set; }
        public Location Location { get; set; }

        [Required]
        public int Rating { get; set; }
        public string? Image { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

    }
}
