﻿using BSBooking.Entity.ConfigModels;
using System.ComponentModel.DataAnnotations;

namespace BSBooking.Entity.Models
{
    public class Location : EntityModel
    {
        [Required]
        public string Name { get; set; }
        public string SubName { get; set; }

        [Required]
        public double Lat { get; set; }

        [Required]
        public double Lon { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
