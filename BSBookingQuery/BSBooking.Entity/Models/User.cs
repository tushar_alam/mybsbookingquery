﻿using BSBooking.Entity.ConfigModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSBooking.Entity.Models
{
    public class User : EntityModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public int UserTypeId { get; set; }
        public UserType UserType { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
