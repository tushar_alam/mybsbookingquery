﻿using BSBooking.Entity.ConfigModels;
using System.ComponentModel.DataAnnotations;

namespace BSBooking.Entity.Models
{
    public class UserType : EntityModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
