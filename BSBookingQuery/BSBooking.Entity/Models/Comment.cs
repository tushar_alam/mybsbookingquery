﻿using BSBooking.Entity.ConfigModels;
using System.ComponentModel.DataAnnotations;

namespace BSBooking.Entity.Models
{
    public class Comment : EntityModel
    {
        [Required]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        public int HotelId { get; set; }
        public Hotel Hotel { get; set; }

        [Required]
        public string CommentText { get; set; }

        public int? ParentCommentId { get; set; }
        public Comment ParentComment { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
