﻿using Microsoft.AspNetCore.Mvc;

namespace BSBookingQuery.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "WEB API IS WORKING...";
        }

    }
}