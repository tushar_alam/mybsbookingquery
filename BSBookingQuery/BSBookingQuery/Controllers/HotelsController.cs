﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.IManagers;
using BSBooking.Repository.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace BSBookingQuery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelsController : ControllerBase
    {
        private readonly IHotelManager _manager;
        private readonly ICommentManager _commentManager;
        private readonly IUnitOfWork _unitOfWork;
        public HotelsController(IHotelManager manager, ICommentManager commentManager, IUnitOfWork unitOfWork)
        {
            _manager = manager;
            _commentManager = commentManager;
            _unitOfWork = unitOfWork;
        }

        [HttpGet("[Action]")]
        public async Task<ResponseModel<IEnumerable<Hotel>>> GetAll()
        {
            var response = new ResponseModel<IEnumerable<Hotel>>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };
            try
            {
                var dataList = await _manager.GetAllAsync();

                response.RspnCode = "OK";
                response.RspnMessage = "Fetched Successfully.";
                response.RspnData = dataList;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }



        [HttpPost("[Action]")]
        public async Task<ResponseModel<IEnumerable<Hotel>>> Search(SearchModel searchModel)
        {
            var response = new ResponseModel<IEnumerable<Hotel>>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };
            try
            {
                var dataList = await _manager.Search(searchModel);

                response.RspnCode = "OK";
                response.RspnMessage = "Fetched Successfully.";
                response.RspnData = dataList;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }


        [HttpGet("[Action]/{id}")]
        public async Task<ActionResult<ResponseModel<Hotel>>> GetById(int id)
        {
            var response = new ResponseModel<Hotel>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };

            try
            {
                var data = await _manager.GetByIdAsync(id);

                if (data == null)
                {
                    response.RspnCode = "ERR";
                    response.RspnMessage = "Not Found.";
                }
                else
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Fetched Successfully.";
                }
                response.RspnData = data;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }

        [HttpPut("[Action]")]
        public async Task<ActionResult<ResponseModel<Hotel>>> Update(HotelModel model)
        {
            var response = new ResponseModel<Hotel>() { RspnCode = "Err", RspnMessage = "Sorry! Update Failed." };

            try
            {
                var entity = new Hotel()
                {
                    Id = model.Id,
                    Name = model.Name,
                    LocationId = model.LocationId,
                    Location = null,
                    Rating = model.Rating,
                    Image = model.Image,

                    CreatedBy = model.CreatedBy,
                    CreateDate = model.CreateDate.Value,
                    LastModifiedBy = "system_user",
                    LastModifiedDate = DateTime.Now
                };


                var isUpdated = _manager.Update(entity);
                if (isUpdated)
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }


        [HttpPost("[Action]")]
        public async Task<ActionResult<ResponseModel<Hotel>>> Create(HotelModel model)
        {
            var response = new ResponseModel<Hotel>() { RspnCode = "Err", RspnMessage = "Sorry! Creation Failed." };

            try
            {
                var entity = new Hotel()
                {
                    Name = model.Name,
                    LocationId = model.LocationId,
                    Location = null,
                    Rating = model.Rating,
                    Image = model.Image,

                    CreatedBy = "system_user",
                    CreateDate = DateTime.Now,
                    LastModifiedBy = "system_user",
                    LastModifiedDate = DateTime.Now
                };

                bool isAdded = _manager.Add(entity);
                if (isAdded)
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Created Successfully.";
                }
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }


        [HttpDelete("[Action]/{id}")]
        public async Task<ActionResult<ResponseModel<Hotel>>> Delete(int id)
        {
            var response = new ResponseModel<Hotel>() { RspnCode = "Err", RspnMessage = "Sorry! Delete Failed." };

            try
            {
                var model = await _manager.GetByIdAsync(id);
                if (model == null)
                {
                    response.RspnCode = "ERR";
                    response.RspnMessage = "Not Found.";
                }

                var commentsList = _commentManager.GetByExprsn(x => x.HotelId == id);
                if (commentsList != null && commentsList.Count > 0)
                {
                    _commentManager.RemoveRangeWait(commentsList);
                }
                _manager.RemoveWait(model);

                bool isDeleted = await _unitOfWork.SaveChangesAsync();
                if (isDeleted)
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Deleted Successfully.";
                }
            }
            catch (Exception)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }
    }
}
