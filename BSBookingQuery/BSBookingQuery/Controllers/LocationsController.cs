﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.IManagers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BSBookingQuery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly ILocationManager _manager;

        public LocationsController(ILocationManager manager)
        {
            _manager = manager;
        }


        [HttpGet("[Action]")]
        public async Task<ResponseModel<IEnumerable<Location>>> GetAll()
        {
            var response = new ResponseModel<IEnumerable<Location>>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };
            try
            {
                var dataList = await _manager.GetAllAsync();

                response.RspnCode = "OK";
                response.RspnMessage = "Fetched Successfully.";
                response.RspnData = dataList;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }
    }
}
