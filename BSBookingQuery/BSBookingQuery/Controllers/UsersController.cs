﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace BSBookingQuery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserManager _manager;

        public UsersController(IUserManager manager)
        {
            _manager = manager;
        }


        [HttpGet("[Action]")]
        public async Task<ResponseModel<IEnumerable<User>>> GetAll()
        {
            var response = new ResponseModel<IEnumerable<User>>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };
            try
            {
                var dataList = await _manager.GetAllAsync();

                response.RspnCode = "OK";
                response.RspnMessage = "Fetched Successfully.";
                response.RspnData = dataList;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }

        [HttpPost("[Action]")]
        public async Task<ResponseModel<User>> CheckValidUser(UserModel userModel)
        {
            var response = new ResponseModel<User>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };
            try
            {
                var data = await _manager.CheckValidUser(userModel);
                if (data == null)
                {
                    response.RspnCode = "Err";
                    response.RspnMessage = "Sorry! username or password is incorrect.";
                }
                else
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Fetched Successfully.";
                }
                response.RspnData = data;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }

    }
}
