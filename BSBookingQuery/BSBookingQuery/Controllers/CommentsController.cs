﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.IManagers;
using Microsoft.AspNetCore.Mvc;

namespace BSBookingQuery.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentManager _manager;
        private readonly IUserManager _userManager;


        public CommentsController(ICommentManager manager, IUserManager userManager)
        {
            _manager = manager;
            _userManager = userManager;
        }

        [HttpGet("[Action]")]
        public async Task<ResponseModel<IEnumerable<Comment>>> GetAll()
        {
            var response = new ResponseModel<IEnumerable<Comment>>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };
            try
            {
                var dataList = await _manager.GetAllAsync();

                response.RspnCode = "OK";
                response.RspnMessage = "Fetched Successfully.";
                response.RspnData = dataList;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }

        [HttpGet("[Action]/{id}")]
        public async Task<ActionResult<ResponseModel<Comment>>> GetById(int id)
        {
            var response = new ResponseModel<Comment>() { RspnCode = "Err", RspnMessage = "Sorry! Fetching Failed." };

            try
            {
                var data = await _manager.GetByIdAsync(id);

                if (data == null)
                {
                    response.RspnCode = "ERR";
                    response.RspnMessage = "Not Found.";
                }
                else
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Fetched Successfully.";
                }
                response.RspnData = data;
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }

        [HttpPut("[Action]")]
        public async Task<ActionResult<ResponseModel<Comment>>> Update(CommentModel model)
        {
            var response = new ResponseModel<Comment>() { RspnCode = "Err", RspnMessage = "Sorry! Update Failed." };

            try
            {
                var entity = new Comment()
                {
                    Id = model.Id,
                    UserId = model.UserId != null ? (int)model.UserId : 0,
                    User = null,
                    HotelId = model.HotelId,
                    Hotel = null,
                    CommentText = model.CommentText,
                    ParentCommentId = model.ParentCommentId,
                    ParentComment = null
                };


                var isUpdated = _manager.Update(entity);
                if (isUpdated)
                {
                    entity.User = _userManager.GetById(entity.UserId);

                    response.RspnCode = "OK";
                    response.RspnMessage = "Updated Successfully.";
                    response.RspnData = entity;
                }
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }


        [HttpPost("[Action]")]
        public async Task<ActionResult<ResponseModel<Comment>>> Create(CommentModel model)
        {
            var response = new ResponseModel<Comment>() { RspnCode = "Err", RspnMessage = "Sorry! Creation Failed." };

            try
            {
                var entity = new Comment()
                {
                    UserId = model.UserId != null ? (int)model.UserId : 0,
                    User = null,
                    HotelId = model.HotelId,
                    Hotel = null,
                    CommentText = model.CommentText,
                    ParentCommentId = model.ParentCommentId,
                    ParentComment = null
                };

                bool isAdded = _manager.Add(entity);
                if (isAdded)
                {
                    entity.User = _userManager.GetById(entity.UserId);
                    response.RspnCode = "OK";
                    response.RspnMessage = "Created Successfully.";
                    response.RspnData = entity;
                }
            }
            catch (Exception ex)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }


        [HttpDelete("[Action]/{id}")]
        public async Task<ActionResult<ResponseModel<Comment>>> Delete(int id)
        {
            var response = new ResponseModel<Comment>() { RspnCode = "Err", RspnMessage = "Sorry! Delete Failed." };

            try
            {
                var model = await _manager.GetByIdAsync(id);
                if (model == null)
                {
                    response.RspnCode = "ERR";
                    response.RspnMessage = "Not Found.";
                }
                bool isDeleted = _manager.Remove(model);
                if (isDeleted)
                {
                    response.RspnCode = "OK";
                    response.RspnMessage = "Deleted Successfully.";
                }
            }
            catch (Exception)
            {
                response.RspnCode = "Err";
                response.RspnMessage = "Sorry! An Internal Exception has occured. Please contact to you system administrator.";
            }
            return response;
        }
    }
}
