import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfigService } from '../shared/services/AppConfigService';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {
     apiClientBasicKey = "MDNDOEFCQTYtQUNGRS00QTJDLTk1REItOEY1MDkyM0Y3OEU2OkU2NjYxMDI2LTlDODgtNENENS1COERCLTg0MjgxNkMxOUFERg==";

    // get apiClientBasicKey() { return this.appConfigService.apiClientBasicKey; }
    constructor(private appConfigService: AppConfigService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


        var userModel = localStorage.getItem("UserModel");
        let now = new Date().getTime();

        if (userModel != undefined && userModel != null && userModel != "") {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${JSON.parse(userModel).access_token}`
                }
            });

            request.headers.append('X-Frame-Options', 'DENY')
                .append('Cache-Control', 'no-cache, no-store, must-revalidate')
                .append('Pragma', 'no-cache')
                .append('Expires', '0')
                .append('If-Modified-Since', '0')
        } else {
            request = request.clone({
                setHeaders: {
                    Authorization: `Basic ${this.apiClientBasicKey}`,
                    Content: `application/x-www-form-urlencoded`
                },
            });

            request.headers.append('X-Frame-Options', 'DENY')
                .append('Cache-Control', 'no-cache, no-store, must-revalidate')
                .append('Pragma', 'no-cache')
                .append('Expires', '0')
                .append('If-Modified-Since', '0')
        }

        return next.handle(request);
    }
}
