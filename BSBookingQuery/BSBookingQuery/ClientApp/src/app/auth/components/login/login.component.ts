import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/shared/models/user-model';
import { AppConfigService } from 'src/app/shared/services/AppConfigService';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  sessionMsg: string = "";
  user: UserModel = new UserModel();

  get brandHeadLineClass() { return this.appConfigService.brandHeadLineClass; }

  constructor(private appConfigService: AppConfigService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    let data = history.state.data;
    if (data) {
      this.sessionMsg = history.state.data.Msg;
    }
  }

  NewLogin(form?: NgForm) {

    this.sessionMsg = "";
    if (form?.invalid) {
      return;
    }
    this.userService.CheckValidUser(this.user).subscribe((data) => {
      if (data != null) {
        if (data.RspnCode == "OK") {
          localStorage.setItem("UserModel", JSON.stringify(data.RspnData));

          if (data.RspnData.UserTypeId == 1) {
            this.router.navigate(['/admin/hotel-setup']);
          } else {
            this.router.navigate(['/show-case/hotel-show-case']);
          }
        }
        else {
          this.sessionMsg = data.RspnMessage;
        }
      } else {
        alert("Sorry! no response from server.")
      }
    })
  }
}
