export class ResponseModel<T> {
   RspnCode: string;
   RspnMessage: string;

   RspnData: T;
}
