import { EntityModel } from "src/app/shared/models/entity-model";
import { LocationModel } from "../../show-case/models/location-model";
import { CommentModel } from "./comment-model";

export class HotelModel extends EntityModel {
    Name: string;
    LocationId: number;
    Location: LocationModel;
    Rating: number;
    Image: string;
    Comments: CommentModel[];
}
