export class SearchModel {
    LocationId: number;
    HotelId: number;
    HotelName: string;
    StartRating: number;
    EndRating: number;

}
