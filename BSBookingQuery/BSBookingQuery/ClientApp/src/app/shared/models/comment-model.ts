import { EntityModel } from "./entity-model";
import { HotelModel } from "./hotel-model";
import { UserModel } from "./user-model";

export class CommentModel extends EntityModel {
    UserId: number;
    User: UserModel;

    HotelId: number;
    Hotel: HotelModel;

    CommentText: string;
    ReplyText: string;

    ParentCommentId: number;
    ParentComment: CommentModel;
    Comments: CommentModel[];
}
