import { CommentModel } from "./comment-model";
import { EntityModel } from "./entity-model";

export class UserModel extends EntityModel {
    Name: string;
    UserName: string;
    Password: string;
    UserTypeId: number;
    UserType: any;
    Comments: CommentModel[];
}
