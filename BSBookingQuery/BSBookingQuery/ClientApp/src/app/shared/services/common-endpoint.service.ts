import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppConfigService } from './AppConfigService';


@Injectable({
  providedIn: 'root'
})
export class CommonEndpoint {

  get baseUrl() { return this.configurations.apiBaseUrl; }
  constructor(private http: HttpClient, private configurations: AppConfigService, private router: Router) { }

  get<T>(apiUrl: string): Observable<T> {
    const endpointUrl = `${this.baseUrl}` + apiUrl;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.get(apiUrl));
      }));
  }

  post<T>(tObject: any, apiUrl?: string): Observable<T> {
    const endpointUrl = `${this.baseUrl}` + apiUrl;
    return this.http.post<T>(endpointUrl, JSON.stringify(tObject), this.requestHeaders)
      .pipe<T>(
        catchError(error => {
          return this.handleError(error, () => this.post(tObject, apiUrl));
        }));
  }

  put<T>(tObject: any, apiUrl?: string): Observable<T> {
    const endpointUrl = `${this.baseUrl}` + apiUrl;
    return this.http.put<T>(endpointUrl, JSON.stringify(tObject), this.requestHeaders)
      .pipe<T>(
        catchError(error => {
          return this.handleError(error, () => this.post(tObject, apiUrl));
        }));
  }

  delete<T>(apiUrl: string): Observable<T> {
    const endpointUrl = `${this.baseUrl}` + apiUrl;
    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.get(apiUrl));
      }));
  }

  handleError(error: { status: number; }, continuation: () => Observable<any>) {
    if (error.status == 401) {
      localStorage.clear();
      this.router.navigate(['/hotel-show-case'], { state: { data: { MsgCode: "Unauthorized", Msg: "Sorry! your current session is over. Please login again." } } });
      return throwError('token expired');
    }
    else {
      return throwError(error.status);
    }
  }

  protected get requestHeaders(): { headers: HttpHeaders | { [header: string]: string | string[]; } } {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json, text/plain, */*'
    });
    return { headers };
  }

}
