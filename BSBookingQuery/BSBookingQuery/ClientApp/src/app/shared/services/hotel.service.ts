import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HotelModel } from 'src/app/shared/models/hotel-model';
import { ResponseModel } from '../models/response-model';
import { SearchModel } from '../models/search-model';
import { AppConfigService } from './AppConfigService';
import { CommonEndpoint } from './common-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  get rootUrl() { return this.configurations.apiRootUrl; }

  constructor(private configurations: AppConfigService, private commonEndpoint: CommonEndpoint) {
  }

  getAll(): Observable<ResponseModel<HotelModel[]>> {
    return this.commonEndpoint.get<ResponseModel<HotelModel[]>>('Hotels/GetAll');
  }

  Search(searchModel: SearchModel): Observable<ResponseModel<HotelModel[]>> {
    return this.commonEndpoint.post<ResponseModel<HotelModel[]>>(searchModel, 'Hotels/Search');
  }

  getById(Id: number): Observable<ResponseModel<HotelModel>> {
    return this.commonEndpoint.get<ResponseModel<HotelModel>>('Hotels/GetById/' + Id);
  }

  create(entity: HotelModel): Observable<ResponseModel<HotelModel>> {
    return this.commonEndpoint.post<ResponseModel<HotelModel>>(entity, 'Hotels/Create');
  }

  update(entity: HotelModel): Observable<ResponseModel<HotelModel>> {
    return this.commonEndpoint.put<ResponseModel<HotelModel>>(entity, 'Hotels/Update');
  }

  delete(entity: HotelModel): Observable<ResponseModel<HotelModel>> {
    return this.commonEndpoint.delete<ResponseModel<HotelModel>>('Hotels/Delete/' + entity.Id);
  }
}
