import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from '../models/response-model';
import { UserModel } from '../models/user-model';
import { AppConfigService } from './AppConfigService';
import { CommonEndpoint } from './common-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  get rootUrl() { return this.configurations.apiRootUrl; }

  constructor(private configurations: AppConfigService, private commonEndpoint: CommonEndpoint) {
  }

  CheckValidUser(entity: UserModel): Observable<ResponseModel<UserModel>> {
    return this.commonEndpoint.post<ResponseModel<UserModel>>(entity, 'Users/CheckValidUser');
  }
}
