import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  private appConfig: any;

  constructor(private http: HttpClient) {
   }

  loadAppConfig() {
    return this.http.get('../../assets/config.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }



  // This is an example property ... you can make it however you want.
  get apiBaseUrl() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    return this.appConfig.apiBaseUrl;
  }

  get apiRootUrl() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    return this.appConfig.apiRootUrl;
  }

  get brandHeadLineClass() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    else if (this.appConfig.bankId == 11) {
      return "bgcb-headerline";
    } else if (this.appConfig.bankId == 10) {
      return "ncc-headerline";
    }
    else if (this.appConfig.bankId == 2) {
      return "mdbl-headerline";
    }
    else if (this.appConfig.bankId == 8) {
      return "smbl-headerline";
    }
    else if (this.appConfig.bankId == 6) {
      return "sbac-headerline";
    }
    else if (this.appConfig.bankId==14) {
      return "pdbl-headerline";
    }
    else if (this.appConfig.bankId == 4) {
      return "bkb-headerline";
    }
    else {
      return "tbl-headerline";
    }
  }
  get brandSubHeadLineClass() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    else if (this.appConfig.bankId == 2) {
      return "card-header mdbl-sub-headline";
    }
    else if (this.appConfig.bankId == 11) {
      return "card-header bgcb-sub-headline";
    } else if (this.appConfig.bankId == 10) {
      return "card-header ncc-sub-headline";
    } else if (this.appConfig.bankId == 8) {
      return "card-header smbl-sub-headline";
    } else if (this.appConfig.bankId == 6) {
      return "card-header sbac-sub-headline";
    } else if (this.appConfig.bankId==14) {
      return "card-header pdbl-sub-headline";
    }
    else if (this.appConfig.bankId == 4) {
      return "card-header bkb-sub-headline";
    }
    else {
      return "card-header tbl-sub-headline";
    }
  }

  get brandBackgroungClass() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    else if (this.appConfig.bankId == 2) {
      return "mdbl-backgroung";
    }
    else if (this.appConfig.bankId == 11) {
      return "bgcb-backgroung";
    } else if (this.appConfig.bankId == 10) {
      return "ncc-backgroung";
    } else if (this.appConfig.bankId == 8) {
      return "smbl-backgroung";
    }
    else if (this.appConfig.bankId == 6) {
      return "sbac-backgroung";
    }
    else if (this.appConfig.bankId==14) {
      return "pdbl-backgroung";
    }
    else if (this.appConfig.bankId == 4) {
      return "bkb-backgroung";
    }
    else {
      return "tbl-backgroung";
    }
  }

  get brandWelcomeClass() {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    else if (this.appConfig.bankId == 2) {
      return "mdbl-welcome-text";
    }
    else if (this.appConfig.bankId == 11) {
      return "bgcb-welcome-text";
    } else if (this.appConfig.bankId == 10) {
      return "ncc-welcome-text";
    }
    else if (this.appConfig.bankId == 8) {
      return "smbl-welcome-text";
    }
    else if (this.appConfig.bankId == 6) {
      return "sbac-welcome-text";
    }
    else if (this.appConfig.bankId==14) {
      return "pdbl-welcome-text";
    }
    else if (this.appConfig.bankId == 4) {
      return "bkb-welcome-text";
    }
    else {
      return "tbl-welcome-text";
    }
  }

  get BrandActiveNavLinkClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 11) {
      return "active-nav-link-bgcb";
    }
    else if (this.appConfig.bankId == 2) {
      return "active-nav-link-mdbl";
    }
    else if (this.appConfig.bankId == 10) {
      return "active-nav-link-ncc";
    }
    else if (this.appConfig.bankId == 8) {
      return "active-nav-link-smbl";
    }
    else if (this.appConfig.bankId == 6) {
      return "active-nav-link-sbac";
    }
    else if (this.appConfig.bankId==14) {
      return "active-nav-link-pdbl";
    }
    else if (this.appConfig.bankId == 4) {
      return "active-nav-link-bkb";
    }
    else {
      return "active-nav-link-tbl";
    };
  }
  get BrandNavLinkClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 11) {
      return "nav-link-bgcb";
    }
    else if (this.appConfig.bankId == 2) {
      return "nav-link-mdbl";
    }
    else if (this.appConfig.bankId == 10) {
      return "nav-link-ncc";
    }
    else if (this.appConfig.bankId == 8) {
      return "nav-link-smbl";
    }
    else if (this.appConfig.bankId == 6) {
      return "nav-link-sbac";
    }
    else if (this.appConfig.bankId==14) {
      return "nav-link-pdbl";
    }
    else if (this.appConfig.bankId == 4) {
      return "nav-link-bkb";
    }
    else {
      return "nav-link-tbl";
    };
  }

  get BrandActiveNavNodeClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 11) {
      return "cdk-tree-node-bgcb mat-tree-node-bgcb ng-star-inserted-bgcb active-nav-node-bgcb";
    }
    else if (this.appConfig.bankId == 2) {
      return "cdk-tree-node-mdbl mat-tree-node-mdbl ng-star-inserted-mdbl active-nav-node-mdbl";
    }
    else if (this.appConfig.bankId == 10) {
      return "cdk-tree-node-ncc mat-tree-node-ncc ng-star-inserted-ncc active-nav-node-ncc";
    }
    else if (this.appConfig.bankId == 8) {
      return "cdk-tree-node-smbl mat-tree-node-smbl ng-star-inserted-smbl active-nav-node-smbl";
    }
    else if (this.appConfig.bankId == 6) {
      return "cdk-tree-node-sbac mat-tree-node-sbac ng-star-inserted-sbac active-nav-node-sbac";
    }
    else if (this.appConfig.bankId==14) {
      return "cdk-tree-node-pdbl mat-tree-node-pdbl ng-star-inserted-pdbl active-nav-node-pdbl";
    }
    else if (this.appConfig.bankId == 4) {
      return "cdk-tree-node-bkb mat-tree-node-bkb ng-star-inserted-bkb active-nav-node-bkb";
    }
    else {
      return "cdk-tree-node-tbl mat-tree-node-tbl ng-star-inserted-tbl active-nav-node-tbl";
    };
  }
  get BrandNavNodeClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 11) {
      return "cdk-tree-node-bgcb mat-tree-node-bgcb ng-star-inserted-bgcb";
    } else if (this.appConfig.bankId == 10) {
      return "cdk-tree-node-ncc mat-tree-node-ncc ng-star-inserted-ncc";
    }
    else if (this.appConfig.bankId == 2) {
      return "cdk-tree-node-mdbl mat-tree-node-mdbl ng-star-inserted-mdbl";
    }
    else if (this.appConfig.bankId == 8) {
      return "cdk-tree-node-smbl mat-tree-node-smbl ng-star-inserted-smbl";
    }
    else if (this.appConfig.bankId == 6) {
      return "cdk-tree-node-sbac mat-tree-node-sbac ng-star-inserted-sbac";
    }
    else if (this.appConfig.bankId==14) {
      return "cdk-tree-node-pdbl mat-tree-node-pdbl ng-star-inserted-pdbl";
    }
    else if (this.appConfig.bankId == 4) {
      return "cdk-tree-node-bkb mat-tree-node-bkb ng-star-inserted-bkb";
    }
    else {
      return "cdk-tree-node-tbl mat-tree-node-tbl ng-star-inserted-tbl";
    };
  }

  get BrandNavTriggerClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 11) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-bgcb";
    } else if (this.appConfig.bankId == 10) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-ncc";
    }
    else if (this.appConfig.bankId == 2) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-mdbl";
    }
    else if (this.appConfig.bankId == 8) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-bgcb";
    }
    else if (this.appConfig.bankId == 6) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-sbac";
    }
    else if (this.appConfig.bankId==14) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-pdbl";
    }
    else if (this.appConfig.bankId == 4) {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-bkb";
    }
    else {
      return "mat-focus-indicator mat-menu-trigger mat-button mat-button-base ng-star-inserted mat-menu-tbl";
    };
  }

  get BrandNavItemClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 11) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-bgcb";
    } else if (this.appConfig.bankId == 10) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-ncc";
    }
    else if (this.appConfig.bankId == 2) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-mdbl";
    }
    else if (this.appConfig.bankId == 8) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-bgcb";
    }
    else if (this.appConfig.bankId == 6) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-sbac";
    }
    else if (this.appConfig.bankId==14) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-pdbl";
    }
    else if (this.appConfig.bankId == 4) {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-bkb";
    }
    else {
      return "mat-focus-indicator mat-menu-item ng-tns-c91-19 ng-star-inserted mat-menu-tbl";
    };
  }

  get BrandContainerClass() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }

    if (this.appConfig.bankId == 2) {
      return "mdbl-container-fluid";
    } 
    else {
      return "container-fluid";
    };
  }


}
