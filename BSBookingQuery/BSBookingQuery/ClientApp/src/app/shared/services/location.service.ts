import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from 'src/app/shared/models/response-model';
import { AppConfigService } from 'src/app/shared/services/AppConfigService';
import { CommonEndpoint } from 'src/app/shared/services/common-endpoint.service';
import { LocationModel } from '../../show-case/models/location-model';

@Injectable({
  providedIn: 'root'
})
export class LocationService {


  get rootUrl() { return this.configurations.apiRootUrl; }

  constructor(private configurations: AppConfigService, private commonEndpoint: CommonEndpoint) {
  }

  getAll(): Observable<ResponseModel<LocationModel[]>> {
    return this.commonEndpoint.get<ResponseModel<LocationModel[]>>('Locations/GetAll');
  }

  getById(Id: number): Observable<ResponseModel<LocationModel>> {
    return this.commonEndpoint.get<ResponseModel<LocationModel>>('Locations/GetById/' + Id);
  }

  save(entity: LocationModel): Observable<ResponseModel<LocationModel>> {
    return this.commonEndpoint.post<ResponseModel<LocationModel>>(entity, 'Locations/Save');
  }

  update(entity: LocationModel): Observable<ResponseModel<LocationModel>> {
    return this.commonEndpoint.post<ResponseModel<LocationModel>>(entity, 'Locations/Update');
  }

  delete(entity: LocationModel): Observable<ResponseModel<LocationModel>> {
    return this.commonEndpoint.post<ResponseModel<LocationModel>>(entity, 'Locations/Delete/');
  }
}
