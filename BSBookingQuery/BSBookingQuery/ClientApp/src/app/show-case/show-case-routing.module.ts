import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HotelDetailViewComponent } from './components/hotel-detail-view/hotel-detail-view.component';
import { HotelShowCaseComponent } from './components/hotel-show-case/hotel-show-case.component';
import { ShowCaseComponent } from './show-case.component';

const routes: Routes = [
  {
    path: '', component: ShowCaseComponent,
    children: [
      { path: '', component: HotelShowCaseComponent },
      { path: 'hotel-detail/:id', component: HotelDetailViewComponent },
      { path: 'hotel-show-case', redirectTo: '', pathMatch: 'full' }
    ]
  },
  { path: '', redirectTo: '/hotel-show-case', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowCaseRoutingModule { }
