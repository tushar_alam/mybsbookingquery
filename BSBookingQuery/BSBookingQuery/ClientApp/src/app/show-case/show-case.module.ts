import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowCaseRoutingModule } from './show-case-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CommentService } from './services/comment.service';
import { HotelDetailViewComponent } from './components/hotel-detail-view/hotel-detail-view.component';
import { HotelShowCaseComponent } from './components/hotel-show-case/hotel-show-case.component';
import { ShowCaseComponent } from './show-case.component';


@NgModule({
  declarations: [
    ShowCaseComponent,
    HotelShowCaseComponent,
    HotelDetailViewComponent
  ],
  imports: [
    CommonModule,
    ShowCaseRoutingModule,
    SharedModule,
  ],
  providers: [
    CommentService
  ]
})
export class ShowCaseModule { }
