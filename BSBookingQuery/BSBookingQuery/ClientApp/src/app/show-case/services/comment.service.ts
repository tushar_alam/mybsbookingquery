import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentModel } from 'src/app/shared/models/comment-model';
import { ResponseModel } from 'src/app/shared/models/response-model';
import { SearchModel } from 'src/app/shared/models/search-model';
import { AppConfigService } from 'src/app/shared/services/AppConfigService';
import { CommonEndpoint } from 'src/app/shared/services/common-endpoint.service';

@Injectable()
export class CommentService {

  get rootUrl() { return this.configurations.apiRootUrl; }

  constructor(private configurations: AppConfigService, private commonEndpoint: CommonEndpoint) {
  }

  getAll(): Observable<ResponseModel<CommentModel[]>> {
    return this.commonEndpoint.get<ResponseModel<CommentModel[]>>('Comments/GetAll');
  }

  Search(searchModel: SearchModel): Observable<ResponseModel<CommentModel[]>> {
    return this.commonEndpoint.post<ResponseModel<CommentModel[]>>(searchModel, 'Comments/Search');
  }

  getById(Id: number): Observable<ResponseModel<CommentModel>> {
    return this.commonEndpoint.get<ResponseModel<CommentModel>>('Comments/GetById/' + Id);
  }

  create(entity: CommentModel): Observable<ResponseModel<CommentModel>> {
    return this.commonEndpoint.post<ResponseModel<CommentModel>>(entity, 'Comments/Create');
  }

  update(entity: CommentModel): Observable<ResponseModel<CommentModel>> {
    return this.commonEndpoint.put<ResponseModel<CommentModel>>(entity, 'Comments/Update');
  }

  delete(entity: CommentModel): Observable<ResponseModel<CommentModel>> {
    return this.commonEndpoint.delete<ResponseModel<CommentModel>>('Comments/Delete/' + entity.Id);
  }
}
