import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentModel } from 'src/app/shared/models/comment-model';
import { UserModel } from 'src/app/shared/models/user-model';
import { HotelService } from 'src/app/shared/services/hotel.service';
import { HotelModel } from '../../../shared/models/hotel-model';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'app-hotel-detail-view',
  templateUrl: './hotel-detail-view.component.html',
  styleUrls: ['./hotel-detail-view.component.css']
})
export class HotelDetailViewComponent implements OnInit {
  userData: UserModel;
  newComment: CommentModel = new CommentModel();
  replyComment: CommentModel = new CommentModel();
  hotelDetail: HotelModel = new HotelModel();

  hotelCommentList: CommentModel[] = [];

  constructor(private hotelService: HotelService,
    private commentService: CommentService,
    private router: Router,
    private activatedroute: ActivatedRoute,) { }

  ngOnInit(): void {
    var hotelId = this.activatedroute.snapshot.paramMap.get("id");
    if (hotelId != null && hotelId != undefined) {
      this.getHotelDetail(Number(hotelId));
    }

    var data = localStorage.getItem("UserModel");
    if (data != null) {
      this.userData = JSON.parse(data);
    }

  }


  getHotelDetail(hotelId: number) {

    this.hotelService.getById(hotelId).subscribe((data) => {
      if (data.RspnCode == "OK") {

        this.hotelDetail = data.RspnData;

        if (this.hotelDetail.Comments.length > 0) {
          this.hotelCommentList = this.hotelDetail.Comments.filter(x => x.ParentCommentId == null);

          if (this.hotelCommentList.length > 0) {
            this.hotelCommentList.forEach(parentComment => {
              parentComment.Comments = this.hotelDetail.Comments.filter(x => x.ParentCommentId == parentComment.Id);
            })
          }

        }



      } else {
        alert(data.RspnMessage);
      }
    })
  }

  onReplyCommentSubmit(parentComment: CommentModel) {
    if (this.userData != null) {

      this.replyComment.UserId = this.userData.Id;
      this.replyComment.HotelId = this.hotelDetail.Id;
      this.replyComment.CommentText = parentComment.ReplyText;
      this.replyComment.ParentCommentId = parentComment.Id;
      parentComment.ReplyText = "";

      this.commentService.create(this.replyComment).subscribe((data) => {
        if (data.RspnCode == "OK") {

          // this.hotelDetail.Comments.find(x => x.Id == parentComment.Id)?.Comments.push(data.RspnData) 
          if (parentComment.Comments == undefined) {
            parentComment.Comments = [];
          }
          parentComment.Comments.push(data.RspnData);

          this.reset();
        } else {
          alert(data.RspnMessage);
        }
      })
    } else {
      alert("Please login to write a comment.");
    }
  }

  onNewCommentSubmit(form?: NgForm) {

    if (this.userData != null) {
      this.newComment.UserId = this.userData.Id;
      this.newComment.HotelId = this.hotelDetail.Id;
      this.commentService.create(this.newComment).subscribe((data) => {
        if (data.RspnCode == "OK") {
          this.hotelCommentList.push(data.RspnData);
          this.reset(form);
        } else {
          alert(data.RspnMessage);
        }
      })
    } else {
      alert("Please login to write a comment.");
    }
  }

  reset(form?: NgForm) {
    this.newComment = new CommentModel();
    this.replyComment = new CommentModel();
    if (form) {
      form.resetForm();
    }
  }
}
