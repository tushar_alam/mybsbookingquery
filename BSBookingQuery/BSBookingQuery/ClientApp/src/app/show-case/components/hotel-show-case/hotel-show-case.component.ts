import { Component, OnInit } from '@angular/core';
import { SearchModel } from 'src/app/shared/models/search-model';
import { HotelService } from 'src/app/shared/services/hotel.service';
import { HotelModel } from '../../../shared/models/hotel-model';
import { LocationModel } from '../../models/location-model';
import { LocationService } from '../../../shared/services/location.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel-show-case',
  templateUrl: './hotel-show-case.component.html',
  styleUrls: ['./hotel-show-case.component.css']
})
export class HotelShowCaseComponent implements OnInit {

  locationList: LocationModel[] = [];

  ratingList: number[] = [1, 2, 3, 4, 5, 6, 7];

  hotelList: HotelModel[] = [];
  searchModel: SearchModel = new SearchModel();

  constructor(private locationService: LocationService,
    private hotelService: HotelService,
    private router: Router) { }

  ngOnInit(): void {
    this.locationService.getAll().subscribe((data) => {
      if (data.RspnCode == "OK") {
        this.locationList = data.RspnData;
      } else {
        alert("No location found.");
      }
    })
    this.loadHotel();
  }


  loadHotel() {
    this.hotelService.Search(this.searchModel).subscribe((data) => {
      if (data.RspnCode == "OK") {
        this.hotelList = data.RspnData;
      } else {
        alert("No Hotel found.");
      }
    })
  }

  reset() {
    this.searchModel = new SearchModel();
    this.loadHotel();
  }


  HotelDetail(id: number) {
    this.router.navigate(['/show-case/hotel-detail/', id]);
  }

}
