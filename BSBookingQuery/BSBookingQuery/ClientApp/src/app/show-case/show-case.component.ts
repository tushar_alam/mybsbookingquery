import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '../shared/models/user-model';

@Component({
  selector: 'app-show-case',
  templateUrl: './show-case.component.html',
  styleUrls: ['./show-case.component.css']
})
export class ShowCaseComponent implements OnInit {

  sessionMsg: string = "";
  userData: UserModel;
  constructor(private router: Router) { }

  ngOnInit(): void {
    var user = localStorage.getItem("UserModel");
    if (user != null) {
      this.userData = JSON.parse(user);
    }

    let data = history.state.data;
    if (data) {
      this.sessionMsg = history.state.data.Msg;
    }
  }

  Login() {
    this.router.navigate(['/auth/login']);
  }

  Logout() {
    localStorage.clear();
    location.reload();
  }

}
