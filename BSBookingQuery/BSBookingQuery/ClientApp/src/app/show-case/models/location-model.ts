import { EntityModel } from "src/app/shared/models/entity-model";

export class LocationModel extends EntityModel {
    Name: string;
    SubName: string;
    Lat: number;
    Lon: number;

    Hotels: any[];
}
