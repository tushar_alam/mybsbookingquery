import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    var data = localStorage.getItem("UserModel");
    if (data == null) {
      this.router.navigate(['/show-case/hotel-show-case'], { state: { data: { MsgCode: "Unauthorized", Msg: "Sorry! your current session is over. Please login again." } } });
    }
  }


  backToDashboard() {
    this.router.navigate(['admin/hotel-setup']);
  }

  Logout() {
    localStorage.clear();
    this.router.navigate(['/show-case/hotel-show-case']);
  }

}
