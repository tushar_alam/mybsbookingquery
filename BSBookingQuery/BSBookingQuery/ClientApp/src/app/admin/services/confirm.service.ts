import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { ResponseModel } from 'src/app/shared/models/response-model';
import { AppConfigService } from 'src/app/shared/services/AppConfigService';
import { CommonEndpoint } from 'src/app/shared/services/common-endpoint.service';

@Injectable()
export class ConfirmService {

    deleted = new Subject<boolean>();
    get rootUrl() { return this.configurations.apiRootUrl; }

    constructor(private configurations: AppConfigService, private commonEndpoint: CommonEndpoint) {
    }

    delete(entity: any, url: string): Observable<ResponseModel<string>> {
        const route = `${this.rootUrl}` + url;
        return this.commonEndpoint.post(route, entity);
    }
}
