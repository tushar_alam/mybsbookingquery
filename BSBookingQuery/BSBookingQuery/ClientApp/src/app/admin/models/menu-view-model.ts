export class MenuViewModel {
    menuId: number;
    parentId: number;
    displayName: string;
    navigateUrl: string;
    ChildMenus: MenuViewModel[] = [];

    constructor(menuId: number, parentId: number, displayName: string, navigateUrl: string) {
        this.menuId = menuId ? menuId : 0;
        this.parentId = parentId ? parentId : 0;
        this.displayName = displayName ? displayName : '';
        this.navigateUrl = navigateUrl ? navigateUrl : '';
    }
}
