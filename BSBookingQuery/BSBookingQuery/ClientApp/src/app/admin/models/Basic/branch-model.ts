
export class BranchModel {
    Id: number;
    Name: string;
    Code: string;
    HoYn: string;
    ActiveYn: string;
    CreatedBy: string;
    CreatedDate: Date;
    LastEditedBy: string;
    LastEditedDate: Date;
}
