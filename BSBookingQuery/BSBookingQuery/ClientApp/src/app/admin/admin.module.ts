import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './components/Settings/dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';

import { HotelComponent } from './components/Basic/hotel/hotel.component';


@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    HotelComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,  
  ],
  providers: [
  ]
})
export class AdminModule { }
