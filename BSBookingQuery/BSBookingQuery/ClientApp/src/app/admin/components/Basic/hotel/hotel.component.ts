import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { HotelModel } from 'src/app/shared/models/hotel-model';
import { LocationModel } from 'src/app/show-case/models/location-model';
import { LocationService } from 'src/app/shared/services/location.service';
import { HotelService } from 'src/app/shared/services/hotel.service';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {

  p: number = 1;

  isEditMode: boolean = false;
  placeHolderImage: string = "";

  hotel: HotelModel = new HotelModel();
  hotelList: HotelModel[] = [];
  locationList: LocationModel[] = [];
  ratingList: number[] = [1, 2, 3, 4, 5, 6, 7];

  constructor(private hotelService: HotelService,
    private locationService: LocationService,
    private router: Router) { }

  ngOnInit(): void {

    this.locationService.getAll().subscribe((data) => {
      if (data.RspnCode == "OK") {
        this.locationList = data.RspnData;
      } else {
        alert("No location found.");
      }
    })

    this.getAll();
  }

  getAll() {

    this.hotelService.getAll().subscribe((data) => {

      if (data.RspnCode == "OK") {
        this.hotelList = data.RspnData;
      } else {
        alert(data.RspnMessage);
      }
    })
  }

  onSubmit(form?: NgForm) {
    if (form?.invalid) {
      return;
    }

    if (this.isEditMode) {
      this.update(form);
    } else {
      this.save(form);
    }
  }

  onUpdate(entity: HotelModel) {

    this.isEditMode = true;
    this.hotel = Object.assign({}, entity);
  }

  onDelete(entity: HotelModel) {
    if (confirm("Are you sure you want to delete this item ?")) {
      this.delete(entity);
    }
  }

  save(form?: NgForm) {

    this.hotelService.create(this.hotel).subscribe((data) => {

      if (data.RspnCode == "OK") {
        alert(data.RspnMessage);
        this.getAll();
        this.reset(form);
      } else {
        alert(data.RspnMessage);
      }
    })
  }

  update(form?: NgForm) {
    if (confirm("Are you sure you want to update it?")) {

      this.hotelService.update(this.hotel).subscribe((data) => {

        if (data.RspnCode == "OK") {
          alert(data.RspnMessage);
          this.getAll();
          this.reset(form);
        } else {
          alert(data.RspnMessage);
        }
      })
    }
  }

  delete(entity: HotelModel) {


    this.hotelService.delete(entity).subscribe((data) => {

      if (data.RspnCode == "OK") {
        alert(data.RspnMessage);
        this.getAll();
      } else {
        alert(data.RspnMessage);
      }
    })
  }

  reset(form?: NgForm) {

    this.isEditMode = false;
    this.hotel = new HotelModel();

    if (form) {
      form.resetForm();
    }
  }


}
