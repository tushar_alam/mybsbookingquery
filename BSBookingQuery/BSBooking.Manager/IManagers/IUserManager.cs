﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.Base;

namespace BSBooking.Manager.IManagers
{
    public interface IUserManager : IBaseManager<User>
    {
        Task<User> CheckValidUser(UserModel user);
    }
}
