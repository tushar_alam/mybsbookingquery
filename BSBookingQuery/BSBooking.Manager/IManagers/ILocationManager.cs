﻿using BSBooking.Entity.Models;
using BSBooking.Manager.Base;

namespace BSBooking.Manager.IManagers
{
    public interface ILocationManager : IBaseManager<Location>
    {

    }
}
