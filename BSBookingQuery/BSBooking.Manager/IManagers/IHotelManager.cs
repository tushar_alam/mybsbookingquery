﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.Base;

namespace BSBooking.Manager.IManagers
{
    public interface IHotelManager : IBaseManager<Hotel>
    {
        Task<IEnumerable<Hotel>> Search(SearchModel searchModel);
    }
}
