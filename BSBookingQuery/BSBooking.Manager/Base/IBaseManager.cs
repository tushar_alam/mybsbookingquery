﻿using System.Linq.Expressions;

namespace BSBooking.Manager.Base
{
    public interface IBaseManager<T> where T : class
    {
        IEnumerable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsync();
        ICollection<T> GetByExprsn(Expression<Func<T, bool>> predicate);
        T GetById(long id);
        Task<T> GetByIdAsync(long id);
        bool Add(T entity);
        Task<bool> AddAsync(T entity);


        bool AddRange(ICollection<T> entities);
        bool Update(T entity);
        bool UpdateRange(ICollection<T> entities);
        bool Remove(T entity);
        bool RemoveRange(ICollection<T> entities);
        void RemoveWait(T entity);
        void RemoveRangeWait(ICollection<T> entities);

        int GetCount();
        Task<int> GetCountAsync();
        void Dispose();
    }
}
