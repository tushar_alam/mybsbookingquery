﻿using BSBooking.Entity.Models;
using BSBooking.Manager.Base;
using BSBooking.Manager.IManagers;
using BSBooking.Repository.Context;

namespace BSBooking.Manager.Managers
{
    public class UserTypeManager : BaseManager<UserType>, IUserTypeManager
    {
        private readonly BookingDbContext _context;
        public UserTypeManager(BookingDbContext context) : base(context)
        {
            _context = context;
        }


    }
}
