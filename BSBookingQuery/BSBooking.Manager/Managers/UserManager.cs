﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.Base;
using BSBooking.Manager.IManagers;
using BSBooking.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace BSBooking.Manager.Managers
{
    public class UserManager : BaseManager<User>, IUserManager
    {
        private readonly BookingDbContext _context;
        public UserManager(BookingDbContext context) : base(context)
        {
            _context = context;
        }


        public async Task<User> CheckValidUser(UserModel userModel)
        {
            try
            {
                var user = await _context.Users.Where(x => x.UserName == userModel.UserName && x.Password == userModel.Password).FirstOrDefaultAsync();
                return user;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
