﻿using BSBooking.Entity.Models;
using BSBooking.Manager.Base;
using BSBooking.Manager.IManagers;
using BSBooking.Repository.Context;

namespace BSBooking.Manager.Managers
{
    public class CommentManager : BaseManager<Comment>, ICommentManager
    {
        private readonly BookingDbContext _context;
        public CommentManager(BookingDbContext context) : base(context)
        {
            _context = context;
        }


    }
}
