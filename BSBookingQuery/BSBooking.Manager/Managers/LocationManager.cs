﻿using BSBooking.Entity.Models;
using BSBooking.Manager.Base;
using BSBooking.Manager.IManagers;
using BSBooking.Repository.Context;

namespace BSBooking.Manager.Managers
{
    public class LocationManager : BaseManager<Location>, ILocationManager
    {
        private readonly BookingDbContext _context;
        public LocationManager(BookingDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
