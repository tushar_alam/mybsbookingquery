﻿using BSBooking.Entity.Models;
using BSBooking.Entity.ViewModels;
using BSBooking.Manager.Base;
using BSBooking.Manager.IManagers;
using BSBooking.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace BSBooking.Manager.Managers
{
    public class HotelManager : BaseManager<Hotel>, IHotelManager
    {
        private readonly BookingDbContext _context;
        public HotelManager(BookingDbContext context) : base(context)
        {
            _context = context;
        }

        public override async Task<Hotel> GetByIdAsync(long id)
        {
            try
            {
                var data = await _context.Hotels
                    .Include(x => x.Location)
                    .Where(x => x.Id == id)
                    .FirstOrDefaultAsync();

                await _context.Entry(data)
               .Collection(c => c.Comments)
               .Query()
               .Include(x => x.User)
               .LoadAsync();

                return data;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override async Task<IEnumerable<Hotel>> GetAllAsync()
        {
            try
            {
                var dataList = await _context.Hotels.Include(x => x.Location).ToListAsync();
                return dataList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Hotel>> Search(SearchModel searchModel)
        {
            try
            {
                var hotelList = _context.Hotels.AsQueryable();
                if (searchModel.LocationId != null)
                {
                    hotelList = hotelList.Where(m => m.LocationId == searchModel.LocationId).AsQueryable();
                }
                if (searchModel.HotelId != null)
                {
                    hotelList = hotelList.Where(m => m.Id == searchModel.HotelId).AsQueryable();
                }
                if (searchModel.HotelName != null)
                {
                    hotelList = hotelList.Where(m => m.Name.ToLower() == searchModel.HotelName.ToLower()).AsQueryable();
                }
                if (searchModel.StartRating != null)
                {
                    hotelList = hotelList.Where(m => m.Rating >= searchModel.StartRating).AsQueryable();
                }
                if (searchModel.EndRating != null)
                {
                    hotelList = hotelList.Where(m => m.Rating <= searchModel.EndRating).AsQueryable();
                }

                return await hotelList.ToListAsync();

            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
